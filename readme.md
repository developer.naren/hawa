# Hawa

This is an app created during spaceappschallenge under #aircheck category

## Technologies Used

* Laravel for Server Side
* Angular App for frontend

## What the app does

* When you open the homepage you can see the list of feeds in the Feeds section, 
* You can post your own post
* What you post in the feed, gets posted in the faebook as well with a hashtag hawa and #d-<location>
* What you post in the facebook, with hastag hawa #hawa #d-<destination>, our application can scan the facebook post in our application

## Todo

* The facebook scan needs to be a scheduled and made more optimized
* More real-time data for the locations are needed.