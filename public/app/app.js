angular.module('SpaceApp', [])
    .factory('Feed',[ '$q', '$http',  Feed  ])
    .controller('FeedCtrl',['Feed',  FeedCtrl ]);


function FeedCtrl( Feed ) {


    var that = this;
    that.disableSubmit = true;
    that.filterStatus = 'all';
    that.feeds = [];

    Feed.getAllStatus().then( function( res ) {

        that.feeds = res;
    });

    that.postStatus = function() {

        var data = { 'feed': that.status };
        Feed.postFeed( data ).then( function( res ) {

            that.status = '';
            return that.feeds.unshift( res )
        });
    }


    that.changeStatus = function() {

        var data = { 'feed': that.status };
        Feed.postFeed( data ).then( function( res ) {

            that.status = '';
            return that.feeds.unshift( res )
        });

    }

    that.submitReply = function( item ) {

        var data = { 'replyStatus': item.replyStatus };
        Feed.replyStatus( data ).then( function( res ) {

            item.replying = false;
            that.status = '';
            return that.feeds.unshift( res )
        });

    }
}


function Feed( $q, $http) {

    var feed = {};

    feed.getCheckFeed = getCheckFeed;
    feed.postFeed = postFeed;
    feed.getAllStatus = getAllStatus;
    feed.replyStatus = replyStatus;

    return feed;

    function replyStatus( data ) {

        return $q( function ( resolve, reject) {

            $http.post('api/reply-status', data ).success( function( response ) {

                resolve( response );
            });
        })

    }

    function getCheckFeed() {

        return $q( function ( resolve, reject) {

            $http.get('api/check-feed').success( function( response ) {

                resolve( response );
            });
        })
    }


    function postFeed( data ) {

        return $q( function ( resolve, reject) {

            $http.post('api/post-status', data ).success( function( response ) {

                resolve( response );
            });
        })
    }

    function getAllStatus( ) {

        return $q( function ( resolve, reject) {

            $http.get('api/all-status' ).success( function( response ) {

                resolve( response );
            });
        })

    }


    function getFeed( ) {

        return $q( function ( resolve, reject) {

            $http.get('api/feed').success( function( response ) {

                resolve( response );
            });
        })

    }


}



