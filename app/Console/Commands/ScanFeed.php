<?php

namespace App\Console\Commands;

use App\Models\UserFeed;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ScanFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hawa:scanfeed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan facebook feed for hawa';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $currentUser;
    private $allUsers;

    public function __construct()
    {
        parent::__construct();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->allUsers = User::all();


        $userAccessTokens = [];

        foreach( $this->allUsers as $user ) {

            $userAccessTokens[ $user->id ] = $user->access_token;
        }
        $this->userAccessTokens = $userAccessTokens;

        foreach( $this->allUsers as $user ) {

            $this->currentUser = $user;
            $feed = $this->makeRequest( );
            $this->parseAndSaveFeed( $feed );

        }



    }


    private function makeRequest( ) {

        $client = new Client();
        $this->response = $client->request('GET', 'https://graph.facebook.com/v2.6/me/posts?access_token=' .  $this->userAccessTokens[ $this->currentUser->id ] );
        return $this->response->getBody()->getContents();

    }

    private function parseAndSaveFeed( $feed ) {

        $feedObj = json_decode( $feed );

        $feedArr = $feedObj->data;
        $userFeedArr = [];

        foreach( $feedArr as $f ) {

            if( !empty( $f->message )) {

                if( str_contains( $f->message, '#hawa') ) {
                    $feedArr = [
                            'feed' => $f->message,
                            'fb_status_id' => $f->id
                        ];
                    if( str_contains( $f->message, "#d-")) {

                        list($waste, $locationWD) = explode( "#d-", $f->message );
                        list( $location, $waste ) = explode( " ", $locationWD );
                        $location = str_replace( '#d-', '', $location );
                        $feedArr['location'] = $location;
                    }

                    $thisFeed = new UserFeed( $feedArr );
                }

            }

            $userFeedArr[] = $thisFeed;
        }

        $this->currentUser->feed()->saveMany( $userFeedArr );

        
    }
}
