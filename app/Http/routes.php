<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@index');

Route::get( 'login', 'FacebookController@login' );

Route::get('handle/facebook','FacebookController@handleFb');


Route::get('scan-feed', "FacebookController@scanFeed");

Route::post('register','Auth\AuthController@postRegister');


Route::get('testing', function( ) {

    $arr = json_decode( file_get_contents( 'http://apps.geoportal.icimod.org/HKHAOD/aeronetjson.aspx' ) );


    foreach( $arr as $a ) {

        $climateArr = [
            'lat' => $a->Lat,
            'lon' =>$a->Lon,
            'value' => $a->Value,
            'location' => $a->Name,
            'acquired_date' => date( 'Y-m-d', strtotime( $a->AcqDate ) ),
            'place_id' => $a->ID
        ];
        \App\Models\ClimateData::create( $climateArr );


    }

});


Route::group(['prefix' => 'api'], function() {


    Route::get('check-feed','ApiController@checkFeed');

    Route::get('feed','ApiController@getFeed');

    Route::get('check-air', 'ApiController@checkAir');

    Route::post('post-status', 'ApiController@postStatus');

    Route::get('all-status', 'ApiController@allStatus');

    Route::post('reply-status','ApiController@replyStatus');

});
