<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Facades\Socialite;
use \Config;
use App\Models\User;
use App\Models\UserFeed;

class FacebookController extends Controller
{
    private $currentUser;


    function login( ) {

        return Socialite::driver('facebook')
            ->scopes([
                'user_about_me',
                'email',
                'user_posts',
                'publish_actions'
                ]
            )
            ->redirect();
    }


    function handleFb() {

        $input = \Illuminate\Support\Facades\Input::all();
        $code = $input['code'];
        $client = new Client();
        $res = $client->request('GET', 'https://graph.facebook.com/v2.3/oauth/access_token?client_id='. Config::get( 'services.facebook.client_id' )
            .'&client_secret='. Config::get( 'services.facebook.client_secret' ) .'&code='.
            $code.'&redirect_uri=' . Config::get( 'services.facebook.redirect' ));
        $accessToken = $res->getBody()->getContents();

        echo $accessToken;

    }

    public function scanFeed()
    {

        $this->allUsers = User::all();


        $userAccessTokens = [];

        foreach( $this->allUsers as $user ) {

            $userAccessTokens[ $user->id ] = $user->access_token;
        }
        $this->userAccessTokens = $userAccessTokens;

        foreach( $this->allUsers as $user ) {

            $this->currentUser = $user;
            $feed = $this->makeRequest( );
            $this->parseAndSaveFeed( $feed );

        }



    }


    private function makeRequest( ) {

        $client = new Client();
        $this->response = $client->request('GET', 'https://graph.facebook.com/v2.6/me/posts?access_token=' .  $this->userAccessTokens[ $this->currentUser->id ] );
        return $this->response->getBody()->getContents();

    }

    private function parseAndSaveFeed( $feed ) {

        $feedObj = json_decode( $feed );

        $feedArr = $feedObj->data;
        $userFeedArr = [];

        foreach( $feedArr as $f ) {

            if( !empty( $f->message )) {

                if( str_contains( $f->message, '#hawa') ) {

                    $feedArr = [
                        'feed' => $f->message,
                        'fb_status_id' => $f->id
                    ];

                    if( str_contains( $f->message, '#hawacheck') ) {

                        $feedArr['is_check'] = 1;
                    }




                    $userFeed = new UserFeed( $feedArr );

                    $this->currentUser->feed()->save( $userFeed );

                    if( str_contains( $f->message, "#d-")) {

                        list($waste, $locationWWaste) = explode( "#d-", $f->message );
                        $locationArr = explode( " ", $locationWWaste );
                        $feedArr['location'] = $location = $locationArr[0];
                        $userFeed->location = $location;
                        $userFeed->save();

                    }
                }


            }


        }




    }





}
