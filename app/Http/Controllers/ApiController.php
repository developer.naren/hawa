<?php

namespace App\Http\Controllers;

use App\Models\ClimateData;
use App\Models\User;
use App\Models\UserFeed;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

use GuzzleHttp\Client;

class ApiController extends Controller
{


    function allStatus() {

        return UserFeed::all();
    }

    public function checkFeed() {

        $location = Input::get('location');

        if( !empty( $location ) ) {
            return UserFeed::forLocation( $location );
        }

        return UserFeed::all();



    }


    public function getFeed() {

        $location = Input::get('location');

        $feed = UserFeed::notChecks()->forLocation( $location )->get();

        return $feed;
    }

    public function checkAir() {

        $location = Input::get('location');

        $feed = ClimateData::forLocation( $location )->get();

        return $feed;

    }

    public function postStatus( Request $request ) {

        $user = User::first();
        $string = $request->get('feed') . " #hawa #d-" . $user->location;

        $feed = $user->feed()->save(  new UserFeed( ['feed' => $string ]) );



        $client = new Client();
        $this->response = $client->request('POST', 'https://graph.facebook.com/v2.6/'. $user->facebook_id .'/feed?access_token=' .  $user->access_token,[

                'form_params' => [
                    'message' => $string
                ]

        ] );
        $facebook = json_decode( $this->response->getBody()->getContents() );
        $feed->fb_status_id = $facebook->id;
        $feed->save();
        return $feed;

    }


    public function replyStatus( Request $request) {

        $user = User::first();
        $string = $request->get('replyStatus') . " #hawa #d-" . $user->location ." #hawa-reply";

        $feed = $user->feed()->save(  new UserFeed( ['feed' => $string ]) );



        $client = new Client();
        $this->response = $client->request('POST', 'https://graph.facebook.com/v2.6/'. $user->facebook_id .'/feed?access_token=' .  $user->access_token,[

            'form_params' => [
                'message' => $string
            ]

        ] );
        $facebook = json_decode( $this->response->getBody()->getContents() );
        $feed->fb_status_id = $facebook->id;
        $feed->save();
        return $feed;

    }








}
