<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ClimateData extends Model
{

    protected $table = 'climate_data';
    protected $fillable = [
        'location',
        'lat',
        'lon',
        'acquired_date',
        'value',
        'place_id'
    ];


    function scopeForLocation( $q, $location ) {
        return $q->whereLocation( $location );
    }
}