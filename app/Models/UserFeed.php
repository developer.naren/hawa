<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class UserFeed extends Model
{

    private $client;
    protected $fillable = [
        'feed',
        'fb_status_id',
        'user_id'.
        'location',
        'is_check'

    ];


    function user() {

        return $this->belongsTo( User::class );
    }

    function scopeChecks( $q ) {


        return $q->whereIsCheck( 1 );
    }


    function scopeNotChecks( $q ) {
        return $q->whereIsCheck( 0 );

    }

    function scopeForLocation( $q, $location ) {
        return $q->whereLocation( $location );
    }



}