<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_feeds', function (Blueprint $table) {
            $table->increments('id');

            $table->text('feed');
            $table->integer('reply_to')->nullable()->index()->unsigned();
            $table->string('location');
            $table->boolean('is_check')->default(0);
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('reported')->default(0);
            $table->string('fb_status_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reply_to')->references('id')->on('user_feeds');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_feeds');
    }
}
