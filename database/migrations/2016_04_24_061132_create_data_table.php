<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climate_data', function (Blueprint $table) {
            $table->increments('id');

            $table->string('lon');
            $table->string('lat');
            $table->date('acquired_date');
            $table->string('value');
            $table->string('location');
            $table->integer('place_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('climate_data');
    }
}
